<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>

    <h3>Sign Up Form</h3>

    <form action= "{{ route('register') }}" method="POST">
        @csrf
        <label for="fname">First Name:</label><br><br>
        <input type="text" id="fname" name="fname"><br><br>
       
        <label for="lname">Last Name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
       
        <label for="jeniskelamin">gender:</label><br><br>
        <input type="radio" name="gender" id="jeniskelamin">Male <br>
        <input type="radio" name="gender" id="jeniskelamin">Female <br>
        <input type="radio" name="gender" id="jeniskelamin">Other <br><br>
       
        <label for="negara">Nationality:</label><br><br>
        <select name="Nationality">
            <option value="Indonesia" id="negara">Indonesia</option>
            <option value="Malaysia" id="negara">Malaysia</option>
            <option value="Singapura" id="negara">Singapura</option>
            <option value="Brunei" id="negara ">Brunei</option>
        </select><br><br>
       
        <label for="Bahasa">Language Spoken:</label><br><br>
        <input type="checkbox" name="Language Spoken" id="Bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken " id="Bahasa">English <br>
        <input type="checkbox" name="Language Spoken" id="Bahasa">Other <br><br>

        <label for="bio">Bio:</label> <br /><br />
        <textarea name="" id="" cols="30" rows="10"></textarea><br />
        <button type="submit">Sign Up</button>

    </form>

</body>
</html>
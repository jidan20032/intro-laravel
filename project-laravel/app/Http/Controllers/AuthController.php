<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showregister()
    {
        return view('register');
    }
    
    public function register(Request $request)
    {
        $firstname = $request->input('fname');
        $lastname = $request->input('lname');

        return redirect()->route('welcome', ['firstname'=> $firstname,'lastname'=> $lastname]);
    }

    public function welcome(Request $request)
    {
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

        return view('welcome', ['firstname'=> $firstname,'lastname'=> $lastname]);
    }
}